﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    //clase que modela la tabla de la base de datos 
    public class EmpleadoET
    {
        public string dui { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string fechaNacimiento { get; set; }
        public string nit { get; set; }
        public string isss { get; set; }
        public string telefono { get; set; }
    }
}
