﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace AccesoDatos
{
    public class EmpleadoAD
    {
        //Este metodo devuelve todos los empleados ingresados en la base 
        public List<EmpleadoET> List_Empleado()
        {
            var empleadoList = new List<EmpleadoET>();

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmpleadoConnection"].ToString())) //el id para el connection string 
                {
                    con.Open();

                    var query = new SqlCommand("SELECT * FROM Empleado order by nombre", con);
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read()) //search in dataReader for all the elements 
                        {

                            var item = new EmpleadoET
                            {

                                apellido = dr["apellido"].ToString(),
                                nombre = dr["nombre"].ToString(),
                                dui = dr["dui"].ToString(),
                                nit = dr["nit"].ToString(),
                                fechaNacimiento = Convert.ToDateTime(dr["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy"),                               
                                telefono = dr["telefono"].ToString(),
                                isss = dr["isss"].ToString(),

                            };


                            //agregar empleados a la lista 
                            empleadoList.Add(item);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                var item = new EmpleadoET
                {

                    apellido = "SIN DATOS",
                    nombre = "Error al consultar"
                   
                };
                empleadoList.Add(item);
                return empleadoList;
            }

            return empleadoList;
        }
        /// <summary>
        /// Busca los empleados mayores de 30 anios y devuelve un dataTable con la informacion 
        /// </summary>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public DataTable busquedaReporte()
        {
            var emp = new EmpleadoET();

            DataTable dt = new DataTable();

            dt.Columns.Add("nombre", typeof(string));
            dt.Columns.Add("apellido", typeof(string));
            dt.Columns.Add("fechaNacimiento", typeof(string));
            dt.Columns.Add("dui", typeof(string));
            dt.Columns.Add("nit", typeof(string));
            dt.Columns.Add("isss", typeof(string));
            dt.Columns.Add("telefono", typeof(string));

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmpleadoConnection"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("select * from Empleado where year(GETDATE()) - year(fechaNacimiento) > 30", con); // buscar los mayores de 30 anios
                   // query.Parameters.AddWithValue("@dui", dui);

                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dt.Rows.Add(dr["nombre"].ToString(), dr["apellido"].ToString(), Convert.ToDateTime(dr["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy"),
                                dr["dui"].ToString(), dr["nit"].ToString(), dr["isss"].ToString(), dr["telefono"].ToString());

                        }



                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return dt;
        }
       


        /// <summary>
        /// Agregar empleados a la base de datos 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public bool AgregarEmpleado(EmpleadoET emp)
        {
            bool result = false;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmpleadoConnection"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("INSERT INTO Empleado( nombre, apellido,dui,fechaNacimiento,nit,isss,telefono) VALUES ( @nombre, @apellido,@dui,@fechaNacimiento,@nit,@isss,@telefono)", con);

                    
                    query.Parameters.AddWithValue("@nombre", emp.nombre);
                    query.Parameters.AddWithValue("@apellido", emp.apellido);
                    query.Parameters.AddWithValue("@dui", emp.dui);
                    query.Parameters.AddWithValue("@fechaNacimiento", Convert.ToDateTime(emp.fechaNacimiento));
                    query.Parameters.AddWithValue("@nit", emp.nit);
                    query.Parameters.AddWithValue("@isss", emp.isss);
                    query.Parameters.AddWithValue("@telefono", emp.telefono);

                    query.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result; //si es true se ingreso el registro exitosamente pero si es false quiere decir que algo fallo 
        }

        public bool ActualizarEmpleado(EmpleadoET emp)
        {
            bool result = false;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmpleadoConnection"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("UPDATE Empleado SET nombre= @nombre, apellido= @apellido,fechaNacimiento=@fechaNacimiento,nit=@nit,isss=@isss,telefono=@telefono" +
                        " where dui=@dui", con);


                    query.Parameters.AddWithValue("@nombre", emp.nombre);
                    query.Parameters.AddWithValue("@apellido", emp.apellido);
                    query.Parameters.AddWithValue("@dui", emp.dui);
                    query.Parameters.AddWithValue("@fechaNacimiento", Convert.ToDateTime(emp.fechaNacimiento));
                    query.Parameters.AddWithValue("@nit", emp.nit);
                    query.Parameters.AddWithValue("@isss", emp.isss);
                    query.Parameters.AddWithValue("@telefono", emp.telefono);

                    query.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result; //si es true se ingreso el registro exitosamente pero si es false quiere decir que algo fallo 
        }

        public bool EliminarEmpleado(string dui)
        {
            bool result = false;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmpleadoConnection"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("DELETE FROM Empleado Where dui=@dui", con);

                    query.Parameters.AddWithValue("@dui", dui);

                    query.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result; //si es true se ingreso el registro exitosamente pero si es false quiere decir que algo fallo 
        }


   




    }
}
