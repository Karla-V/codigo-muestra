﻿using Entidad;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrupoGD_Test.Controllers
{
    public class EmpleadoController : Controller
    {

        private EmpleadoLN empBL = new EmpleadoLN();


        public JsonResult AgregarEmpleado(EmpleadoET emp)
        {
            bool response = empBL.AgregarEmpleado(emp);



            return Json(response);  //true = exito, false = fallo
        }


        public JsonResult EliminarEmpleado(string dui)
        {
            bool response = empBL.EliminarEmpleado(dui);



            return Json(response);  //true = exito, false = fallo
        }


        public JsonResult ActualizarEmpleado(EmpleadoET emp)
        {
            bool response = empBL.ActualizarEmpleado(emp);

            return Json(response);  //true = exito, false = fallo
        }

    }
}