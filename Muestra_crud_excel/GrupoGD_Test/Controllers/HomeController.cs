﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidad;
using LogicaNegocio;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace GrupoGD_Test.Controllers
{
    public class HomeController : Controller
    {
        private EmpleadoLN empBL = new EmpleadoLN();

        [HttpGet]
        public ActionResult Index()
        {
            List<EmpleadoET> empList = empBL.empleadoList(); //buscar todos los empleados

            

            ViewBag.empList = empList; //llena la lista con los empleados de la base de datos            

            return View();
        }






        [HttpPost]
        public ActionResult Index(HttpPostedFileBase archivo, string consulta)
        {

            string ruta1 = System.IO.Path.GetDirectoryName(archivo.FileName);
            string ruta = "";
            if (archivo != null)
            {
                ruta = Server.MapPath("~/Temp/");
                ruta += archivo.FileName;
                archivo.SaveAs(ruta);
            }

            bool response = empBL.ImportarArchiv(ruta);
            ViewBag.consulta = false;

            List<EmpleadoET> empList = empBL.empleadoList();
            ViewBag.empList = empList;

            return View("Index");


        }

        public JsonResult ExportarArchivo()
        {
            string ruta = Server.MapPath("~/Temp/");
            string nombre_archivo= "evaluación_" + DateTime.Now.Day+"-"+ DateTime.Now.Month+"-"+DateTime.Now.Year+"_"+ DateTime.Now.Minute + DateTime.Now.Second + ".xlsx";
            ruta += nombre_archivo;

            

            bool result= empBL.ExportarArchivo(ruta);

            if (result)
            {
                return Json(nombre_archivo);
            }
            else
            {
                return Json(result);
            }

              //true = exito, false = fallo
        }






        
    }
}