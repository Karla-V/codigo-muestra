﻿
(function (w, d, $) {
    w.main = {
        init: function () {          
            setEvents();
        },

        llamadaAjax: llamadaAjax,
        validaVacio: validaVacio,
        validarCampos: validarCampos,
        llamadaAjax2: llamadaAjax2
    };

    function setEvents() {

        //alert("hola");

        $("#txtDui").mask("99999999-9");/// mascara del campo dui
        $("#txtNit").mask("9999-999999-999-9");
        $("#txtIsss").mask("999999999");
        $("#txtTelefono").mask("+(999)-9999-9999");


        //set datetimepicker format 
        $('#datetimepicker1').datetimepicker({
            format: 'dd/mm/yyyy',
            minView: 'month'
        });


        $("#btn_agregar").click(function () {
           
            var campos = validarCampos();

            if (validaVacio(campos)) {
                var sdate = $("#txt_dtpicker1").val();
                var hdate = Date(sdate);


                //guardar la informacion de la vista en un objeto 
                var params = {
                    dui: $("#txtDui").val(),
                    nombre: $("#txtNombre").val(),
                    apellido: $("#txtApellido").val(),
                    fechaNacimiento: sdate,
                    nit: $("#txtNit").val(),
                    isss: $("#txtIsss").val(),
                    telefono: $("#txtTelefono").val(),


                };

                //hacer post usando ajax y enviar la informacion al controlador 
                llamadaAjax('/Empleado/AgregarEmpleado', params).success(function (data) {
                    if (data) {

                        location.reload(true);
                    } else {
                        alert("Error al guardar, es posible que ya exista un empleado con este numero de dui");
                    }



                });
            } else {
                alert(campos);
            }
            
            

        });

        $("#btn_modificar").click(function () {
            var campos = validarCampos();
           
          
           
            if (validaVacio(campos)) {
                var sdate = $("#txt_dtpicker1").val();
                var hdate = Date(sdate);


                //guardar la informacion de la vista en un objeto 
                var params = {
                    dui: $("#txtDui").val(),
                    nombre: $("#txtNombre").val(),
                    apellido: $("#txtApellido").val(),
                    fechaNacimiento: sdate,
                    nit: $("#txtNit").val(),
                    isss: $("#txtIsss").val(),
                    telefono: $("#txtTelefono").val(),


                };

                //hacer post usando ajax y enviar la informacion al controlador 
                llamadaAjax('/Empleado/ActualizarEmpleado', params).success(function (data) {
                    if (data) {

                        location.reload(true);
                    } else {
                        alert("Error al actualizar, favor verificar que los datos tengan el formato correcto");
                    }



                });
            } else {
                alert(campos);
            }


           

        });

        $("#btn_eliminar").click(function () {

            $("#consulta").val("s");

            //guardar la informacion de la vista en un objeto 
            var params = {
                dui: $("#txtDui").val()

            };

            //hacer post usando ajax y enviar la informacion al controlador 
            llamadaAjax('/Empleado/EliminarEmpleado', params).success(function (data) {
                if (data) {

                    location.reload(true);
                } else {
                    alert("Error al eliminar");
                }



            });

        });

        $("#btn_exportar").click(function () {


            //hacer post usando ajax y enviar la informacion al controlador 
            llamadaAjax('/Home/ExportarArchivo').success(function (data) {
                if (!data) {
                    alert("Error al exportar excel");
                } else {

                    alert("El archivo se creo en la carpeta Temp con el nombre " + data);
                }
            });

        });

        //$("#btn_importar").click(function () {
        //    $("#consulta").val("n");

        //    //hacer post usando ajax y enviar la informacion al controlador 
        //    llamadaAjax('/Home/Importar').success(function (data) {
        //        if (!data) {
        //            alert("Error al importar excel");
        //        } else {

        //            location.reload(true);
        //        }
        //    });

        //});

        //$("#btn_reporte").click(function () {


        //    //hacer post usando ajax y enviar la informacion al controlador 
        //    llamadaAjax('/Reporte/Index');

        //});

     


        $(".editar").click(function () {

            //console.log(this.id);       // Outputs the answer

            var tablaAcep = document.getElementById("tblEmpleado"), rows = tablaAcep.rows, rowcount = rows.length, r,
                cells, cellcount, c, cell;
            var datos = new Array(1);
            datos[0] = new Array(7);
            for (r = 0; r < rowcount; r++) {
                cells = rows[r].cells;
                cellcount = cells.length;
                
                cell = cells[3];
                text = (cell.textContent || cell.innerText);

                if (this.id == text) {
                    for (c = 0; c < cellcount; c++) {
                        cell = cells[c];
                        text = (cell.textContent || cell.innerText);
                        datos[0][c] = text;
                    }
                }                
            }

            $("#txtNombre").val(datos[0][0]);
            $("#txtApellido").val(datos[0][1]);
            $("#txt_dtpicker1").val(datos[0][2]);
            $("#txtDui").val(datos[0][3]);
            $("#txtDui").attr("readonly", true);
            $("#txtNit").val(datos[0][4]);
            $("#txtIsss").val(datos[0][5]);
            $("#txtTelefono").val(datos[0][6]);
            

            //console.log(datos);
            //console.log(datos.length);
            //console.log(datos[0][1]);

        });



    };//fin setEvents


    


    ///hacer ajax
    function llamadaAjax(url, parameter, before) {
        return $.ajax({
            "async": true,
            "method": "POST",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache"
            },
            "url": url,
            "processData": false,
            "data": JSON.stringify(parameter),
            "dataType": "json",
            "beforeSend": function () { if (before) { before(); } }
        });
    }

    function llamadaAjax2(url, parameter, before) {
        return $.ajax({
            "async": true,
            "method": "GET",
            "crossDomain": true,
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache"
            },
            "url": url,
            "processData": false,
            "data": JSON.stringify(parameter),
            "dataType": "json",
            "beforeSend": function () { if (before) { before(); } }
        });
    }

    function validarCampos() {

        var campos = "Debe llenar los siguientes campos: ";

        var vacios = false;

        var sdate = $("#txt_dtpicker1").val();

        if (validaVacio(sdate)) {
            campos = campos + "Fecha Nacimiento  ";
            vacios = true;
        }

        if (validaVacio($("#txtDui").val())) {
            campos = campos + "DUI  "
            vacios = true;
        }

        if (validaVacio($("#txtNit").val())) {
            campos = campos + "NIT  "
            vacios = true;
        }

        if (validaVacio($("#txtNombre").val())) {
            campos = campos + "Nombre  "
            vacios = true;
        }

        if (validaVacio($("#txtApellido").val())) {
            campos = campos + "Apellido  "
            vacios = true;
        }

        if (validaVacio($("#txtIsss").val())) {
            campos = campos + "ISSS  "
            vacios = true;
        }

        if (validaVacio($("#txtTelefono").val())) {
            campos = campos + "Telefono  "
            vacios = true;
        }

        if (vacios) {
            return campos;
        } else {
            campos = "";
            return campos;
        }



    }

    function validaVacio(valor) {
        valor = valor.replace("&nbsp;", "");
        valor = valor == undefined ? "" : valor;
        if (!valor || 0 === valor.trim().length) {
            return true;
        }
        else {
            return false;
        }
    }



    $(d).ready(main.init);
})(window, document, jQuery);