﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidad;
using SpreadsheetLight;
using System.Data;

namespace LogicaNegocio
{
    public class EmpleadoLN
    {
        private EmpleadoAD empDA = new EmpleadoAD();

        ///Lista de todos los empleados
        public List<EmpleadoET> empleadoList()
        {
            return empDA.List_Empleado();
        }
        

        /// <summary>
        /// Guarda la informacion del empleado 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public bool AgregarEmpleado(EmpleadoET emp)
        {
            return empDA.AgregarEmpleado(emp);
        }

        /// <summary>
        /// Actualizar empleados 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public bool ActualizarEmpleado(EmpleadoET emp)
        {
            return empDA.ActualizarEmpleado(emp);
        }

        /// <summary>
        /// Eliminar empleado 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public bool EliminarEmpleado(string dui)
        {
            return empDA.EliminarEmpleado(dui);
        }

        public bool ImportarArchiv(string ruta)
        {
            //string ruta2 = @"" + ruta;
            bool res = true;

             SLDocument sl = new SLDocument(ruta);

            int i = 2; //inicar desde la fila 2

            List<EmpleadoET> empList = new List<EmpleadoET>();

            while (!string.IsNullOrEmpty(sl.GetCellValueAsString(i,1)))
            {
                EmpleadoET item = new EmpleadoET();
                item.nombre = sl.GetCellValueAsString(i, 1);
                item.apellido = sl.GetCellValueAsString(i, 2);
                DateTime fechaNac = sl.GetCellValueAsDateTime(i, 3);
                item.fechaNacimiento = fechaNac.ToString("dd/MM/yyyy");
                item.dui = sl.GetCellValueAsString(i, 4);
                item.nit = sl.GetCellValueAsString(i, 5);
                item.isss = sl.GetCellValueAsString(i, 6);
                item.telefono = sl.GetCellValueAsString(i, 7);

                empList.Add(item);
                i++;
            }

            foreach (var item in empList)
            {
               bool resultado= empDA.AgregarEmpleado(item);

                if (!resultado)
                {
                    res = false;
                    break;
                }
            
            }


            return res;
        }

        public bool ExportarArchivo(string ruta)
        {            

            SLDocument sl = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            dt.Columns.Add("nombre", typeof(string));
            dt.Columns.Add("apellido", typeof(string));
            dt.Columns.Add("fechaNacimiento", typeof(string));
            dt.Columns.Add("dui", typeof(string));
            dt.Columns.Add("nit", typeof(string));
            dt.Columns.Add("isss", typeof(string));
            dt.Columns.Add("telefono", typeof(string));

            List<EmpleadoET> lista_Empleado = empDA.List_Empleado();

            foreach (var item in lista_Empleado)
            {
               

                dt.Rows.Add(item.nombre, item.apellido,item.fechaNacimiento, item.dui, item.nit, item.isss, item.telefono);
            }

            try
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SaveAs(ruta);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           



        }


        public DataTable busquedaReporte()
        {
            return empDA.busquedaReporte();
        }





    }
}
